package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by mche618 on 20/03/2017.
 */
public class Elephant extends Animal{

    private static final String name = "Elephant";

    private int value;
    private  int max;

    public Elephant() {
        value = 1500;
        max = 4000;
    }

    public void feed() {
        if (value < max) {
            value += (max-value)/2;
        }
    }

    public int costToFeed() {
        return 120;
    }
    public int getMax(){
        return  max;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return name + " - $" + value+ " and maximum potential vlue is "+max;
    }
}
